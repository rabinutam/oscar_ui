'use strict';

/* node powered UI app */

var express = require('express');
var app = express();
//app.directory = __dirname;


/* Static Content */
app.use(express.static(__dirname + '/public'));
app.use('/bower_components', express.static(__dirname + '/bower_components'));

//require('./applib/routes/index')(app);
/* send this to routes later */
app.get('/', function(req, res){
	console.log('serving home page');
	res.sendFile(__dirname + 'public/index.html');
});

module.exports = app;
