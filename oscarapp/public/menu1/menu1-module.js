'use strict';

(function () {
    var dependencies = [];
    angular.module('menu1Module', dependencies)

    .controller('menu1Controller', ['$scope', '$http', '$q', function($scope, $http, $q) {

        var api_root = "http://52.36.160.40/";

        /* default */
        var initVoteData = {
                movie_id: null,
                age: null,
                gender: null,
                zipcode: null
            }
        $scope.showVotesButtonDisabled = false; // change it to true
        $scope.displayVotesView = false;
        $scope.movies = [];
        $scope.votes = [];
        $scope.voteData = initVoteData;
        $scope.showMessage = false;

        /* first call at page load */
        getMovies(api_root);
        getVotes(api_root);

        /* button: Vote/Submit */
        $scope.voteMovie = function() {
            console.log($scope.voteData);
            postVote(api_root, $scope.voteData);
            $scope.voteData = initVoteData;
            $scope.voteForm.$setPristine();
            $scope.showMessage = true;
        }

        /* button: Show Votes */
        $scope.showVotes = function() {
            $scope.displayVotesView = true;
            getVotes(api_root);
            $scope.showVotesButtonDisabled = false;
        }

        /* POST votes API call */
        function postVote(api_root, data) {
            var url = api_root + "votes" + '/';
            var mockData = {
                movie_id: 1,
                age: 18,
                gender: 'M',
                zipcode: '75080'
            }
            console.log("request data", data);
            // $http.post(url, JSON.stringify(data))
            $http.post(url, data)
            .then(function(response){
                console.log("success calling " + url);
                console.log('response.data', response.data);
            }, function(response){
                console.log("error calling " + url);
            })
                
        }

        /* GET movies API call */
        function getMovies(api_root) {
            var url = api_root + "movies" + '/';
            $http.get(url)
            .then(function(response){
                console.log("success calling " + url);
                console.log('response.data', response.data);
                $scope.movies = response.data.movies
            }, function(response){
                console.log("error calling " + url);
            })
        }

        /* GET votes API call */
        function getVotes(api_root) {
            var url = api_root + "votes" + '/';
            $http.get(url)
            .then(function(response){
                console.log("success calling " + url);
                console.log('response.data', response.data);
                $scope.votes = response.data.votes
            }, function(response){
                console.log("error calling " + url);
            })
        }


    }])

})();
