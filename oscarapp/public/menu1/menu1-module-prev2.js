'use strict';

(function (){
	var dependencies = [];
	angular.module('menu1Module', dependencies)

	.controller('menu1Controller', ['$scope', '$http', 'data', function($scope, $http, data) {
		var searchResults = [];

		var movies = data.movies;
		var votes = data.votes;

		$scope.mainBtnDisabled = false;
		$scope.movies = data.movies;

		$scope.callSampleApi = function() {
			// TODO API call to get movies from proj database and not omdb
			console.log('in $scope.callSampleApi');
			// getMovieList();
			$scope.mainBtnDisabled = true;
		}

		$scope.voteMovie = function(item) {
			// TODO API call to update voteCount
			item.voteCount = item.voteCount + 1;
		}

		var getMovieList = function() {
			var movies;
			$http.get("http://52.36.160.40/movies/")
			.success(function(data, status, headers, config){
				console.log('response data', data.movies);
				// TODO API call to get the voteCount
				data.voteCount = 0;
				$scope.searchResults = data.movies;
			})
			.error(function(data, status, headers, config){
				console.log("error calling /movies/");
			})
		}
			
	}])

})();
