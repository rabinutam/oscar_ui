'use strict';

(function (){
	var dependencies = [];
	angular.module('menu1Module', dependencies)

	.controller('menu1Controller', ['$scope', '$http', function($scope, $http) {
		var nominees = ["The Big Short", "Bridge of Spies", "Brooklyn", "Mad Max: Fury Road",
			"The Martian", "The Revenant", "Room", "Spotlight"];
		// var search = "ram";
		var searchResults = [];

		$scope.mainBtnDisabled = false;

		$scope.callSampleApi = function() {
			// TODO API call to get movies from proj database and not omdb
			console.log('in $scope.callSampleApi');
			nominees.forEach(function(element, index, array){
				getMovieList(element);
			})
			$scope.mainBtnDisabled = true;
			console.log('all movies', searchResults);
			$scope.searchResults = searchResults;
		}

		$scope.voteMovie = function(item) {
			// TODO API call to update voteCount
			item.voteCount = item.voteCount + 1;
		}

		var getMovieList = function(search) {
			$http.get("http://52.36.160.40/movies/")
			.success(function(data, status, headers, config){
				console.log('response data', data);
				// TODO API call to get the voteCount
				// data.voteCount = 0;
				searchResults.push(data);
			})
			.error(function(data, status, headers, config){
				console.log("error calling http://omdbapi.com/");
			})
		}
		var xgetMovieList = function(search) {
			// $http.get("http://omdbapi.com/?t=" + search + "&tomatoes=true&plot=full")
			$http.get("http://omdbapi.com/?t=" + search + "&plot=full")
			.success(function(data, status, headers, config){
				console.log('response data', data);
				// TODO API call to get the voteCount
				// data.voteCount = 0;
				searchResults.push(data);
			})
			.error(function(data, status, headers, config){
				console.log("error calling http://omdbapi.com/");
			})

		}
			
	}])

})();
