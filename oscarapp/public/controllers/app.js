'use strict';

(function(){
    var dependencies = [
		'menu1Module',
        'ngRoute'
    ];

    angular.module('oscarvote', dependencies)
        .controller('appController', function($rootScope) {
        })

        .config(['$routeProvider', function($routeProvider){
			$routeProvider
            	.when('/home', {
                	// templateUrl: 'home/home-tpl.html',
                	// controller: 'homeController'
                	templateUrl: 'menu1/menu1-tpl.html',
                	controller: 'menu1Controller'
            	})
            	.when('/menu1', {
                	templateUrl: 'menu1/menu1-tpl.html',
                	controller: 'menu1Controller'
            	})
            	.when('/menu2', {
                	templateUrl: 'menu2/menu2-tpl.html',
                	controller: 'menu2Controller'
            	})
            	.otherwise({
                	redirectTo: '/home'
            	});
        }
    ]);
})();
