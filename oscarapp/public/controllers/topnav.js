'use strict';

(function(){
    angular.module('oscarvote')
        .controller('topNavController', ['$scope', '$rootScope', '$route',
        function($scope, $rootscope, $route) {
            $rootScope.$on('$routeChangeSuccess', function () {
                $scope.activeTab = $route.current.activeTab;
            });
        }
    ]);
})();
