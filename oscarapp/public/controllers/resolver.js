'use_strict';

(function() {
	var dependencies = [];
	angular.module('Resolver', dependencies)

	.provider('MovieResolver', function() {
		new Resolver(this, [
			{key: 'movies', path:"http://52.36.160.40/movies/"},
			{key: 'votes', path:"http://52.36.160.40/votes/"}
		]);
	})

})();
